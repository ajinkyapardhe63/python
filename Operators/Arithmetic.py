
x=2;
y=5;

print(x+y);         #7

print(x-y);         #-3

print(x/y);         #0.4 (this is float division it returns float value)

print(x*y);         #10

print(x%y);         #2

print(x//y);        #0 (returns floor value which integer)

print(x**y);        #32 (returns exponent) 2 exopenent of 5 means 2**5

