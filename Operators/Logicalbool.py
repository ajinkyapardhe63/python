#Logical boolean returns boolean value . logical boolean have boolean operands

x=True;
y=False;

print(x and y);             #true and false = false
print(x or y);              #true or false = true
print(not x);               #not true = false
print(not y);               #not false = true

