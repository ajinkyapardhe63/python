# Identity operators are 'is' , 'is not' . it returns Boolean Value.

x=10;
y=10;

print(id(x));               #1000
print(id(y));               #1000
print(x is y);              #True

y=25;

print(id(x));               #1000
print(id(y));               #2000
print(x is y);              #False
print(x is not y );         #True

