#In logicallogical operator we do operation on variable other than boolean eg int float


a=10;
b=20;

print(a and b);        #in the and operators complier "checks if both are true than it executes" therefore a is checked and b is checked and returns b which is 20 
print(a or b);          #in the or operators complier "checks if both and if any one is true than it executes" therefore a is checked and b is checked and returns a which is 10
print(not a);           #"not a" means "not 10" can also be said as "not true" therefore "false" is return

a=0;
print(a and b);         # 0 represents false else represents true .both should be true .both are not true therefore 0 is return
print(a or b);          # 0 represents false else represents true .any one should be true .b is true therefore 20 is return
print(not a);           # 0 represents false and not false is true therefore true is reurn
