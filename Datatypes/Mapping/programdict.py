#Dictionary is used "mostly in python" . Dictionary "stores key value pair" data (K,V)
#Dictionary is "mutable"we can change the data which is present in the dictionary

player = {7:"Mahi",18:"Virat",45:"Rohit"};
print(player);                  #{7:'Mahi',18:'Virat',45:'Rohit'};
print(type(player))             #<class 'dict'>

player[7] = "MS Dhoni";         # to change the data in the dictionary we have to give key of which we have to change the value
print(player);                  #{7:'MS Dhoni',18:'Virat',45:'Rohit'};
