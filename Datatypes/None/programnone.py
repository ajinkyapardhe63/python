# if we dont want to initialize the variable but we want to only declare the variable that time we use none datatype

x = None;               
print(x);               #None
print(type(x));         #<class 'NoneType'>

x=12;
print(x);               #12
x="ajinkya";
print(x);               #ajinkya
