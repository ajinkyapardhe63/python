complexdata=10+5j;              #complex numbers are the combinaton of real number and imaginary number here 10 is real number and 5j is imaginary number
print(complexdata);             #(10+5j)
print(type(complexdata));       #<class 'complex'>
