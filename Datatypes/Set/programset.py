#Set has "unique data" stored in it . it does "not allow duplicate data" to be stored therefore set" removed all the duplicate" data present in it and "store and all unique data".
#set is "immutable" we can't change the data which is present in the set
#set is represented by "{} curly brackets"

setdata = {10,10,20,15,20,35};
print(setdata);                 #{10,35,20,15} set store or give "output of set randomly not sequencly"
print(type(setdata));           #<class 'set'>

setdata[2]=50;                  #TypeError: 'set' object does not support item assignment (immutable)

