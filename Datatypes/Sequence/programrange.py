#range is a datatype in which we give a rang of variables or number to print it we use forloop



x=range(21,38);             #range(21,38) is from 21 till 37 therefore 38 will not be printed in forloop
print(x);                   #range(21,38)
print(type(x));             #<class 'range'>

for i in x:
    print(i);               #21,22,23.......................,37
