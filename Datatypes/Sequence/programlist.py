#list "allows dupicate" data to store in it . list is reoresented by "[] square brackets"
#list is "mutable" it means we can change data which is present in the list
#we can store any kind of data in list

emplist=[18,"Ashish",25.5,11,"kanha",35.5,11];
print(emplist);             #[18,'Ashish',25.5,11,'kanha',35.5,11];
print(type(emplist));       #<class 'list'>

emplist[4]="Rahul";
print(emplist);             #[18,'Ashish',25.5,11,'Rahul',35.5,11];
