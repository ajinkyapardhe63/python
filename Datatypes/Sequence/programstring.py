#There are 4 ways to initialize a string

#way-1

friend1 = "kanha";
print(friend1);
print(type(friend1));

#way-2

friend2 = 'Ashish';
print(friend2);
print(type(friend2));

#way-3

friend3 = '''Rahul''';
print(friend3);
print(type(friend3));

#way-4 

friend4 = """shashi""";
print(friend4);
print(type(friend4));
