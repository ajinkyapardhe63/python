#tuple "allows dupicate" data to store in it . list is reoresented by "() round brackets"
#list is "immutable" it means we cannot change data which is present in the tuple
#we can store any kind of data in tuple 


emplist=(18,"Ashish",25.5,11,"kanha",35.5,11);
print(emplist);             #(18,'Ashish',25.5,11,'kanha',35.5,11);
print(type(emplist));       #<class 'tuple'>

emplist[4]="Rahul";
print(emplist);             #TypeError: 'tuple' object does not support item assignment 
