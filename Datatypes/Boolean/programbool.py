
data1=True;
print(data1);               #true
print(type(data1));         #<class 'bool'>

data2=False;
print(data2);               #false
print(type(data2));         #<class 'bool'>

x=10;
y=20;
print(x==y);                #false

ans=data1+data2;            #it means ans = True+False  ie  ans = 1+0 ie ans = 1
print(ans);                 #1
